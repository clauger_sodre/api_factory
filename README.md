# Api_factory

Api factory

## Framworks utilizados
- spring boot
- Hibernate
- liquibase
- Swagger

## Database
- Postgres (Produção)
- H2 (Homologação)

## Ferramentas
- Docker
- DockerCompose
- Maven

## Getting started

usuário padrão
```
{
  "id":"1",
  "password": "123456",
  "username": "user",
  "admin": "true"
}
```

## Documentação com Swagger
http://localhost:8085/swagger-ui.html


buscar um novo token com post no endpoint 
```
http:localhost:8085/auth/authenticate
{
 
  "password": "123456",
  "username": "user"
}
```
ou Curl
```
curl -X POST "http://localhost:8085/auth/authenticate" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"password\": \"123456\",  \"username\": \"user\"}"
```



## Funções não implementadas


## Regras de gerenciamento do workspace do usuário.
criar, listar, atualizar e deletar Especificações de API que estiverem em Workspaces aos quais pertençam-> Não implementado, o usuário pode realizar as operações nas APIs de qualquer workspace -- falta implementar consultas no banco para validar as regras de gerenciamento

## Para rodar o projeto
```
na pasta raiz do projeto
mvn package
docker-compose build
docker-compose up
```

